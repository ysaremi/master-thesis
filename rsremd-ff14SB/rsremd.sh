#!/bin/bash

### Repulsive Scaling Replica Exchange

# - This script runs via 'bash rsremd.sh $protein_name'
# - Check that your $AMBERHOME variable points to your amber20 directory.
# - You will have multiple output files needed for the next step
# - Your final output file will be remd_1.nc - remd_16.nc
#
# Created by Yasmin Saremi (y.saremi@tum.de) - 
# Physics Department T38, Technical University of Munich, Garching, Germany.


#### Variables ####
protein_name=$1

source $AMBERHOME/amber.sh

#### Go To Output Directory ####
mkdir ${protein_name}
mv ${protein_name}.pdb ${protein_name}
cd ${protein_name}

#### Retrieve Mask inforamtion ####
mask=../mask.info
r_mask=$( head -1 ${mask})
l_mask=$( tail -1 ${mask})

#### Pdb4Amber ####
pdb4amber -y -i ${protein_name}.pdb -o myprotein.pdb

#### Tleap ####
python3 ../leap.py myprotein

#### HMass Repartition ####
python3 ../repart.py myprotein.prmtop myprotein_hrpt.prmtop

#### Creating Parm for Replica Exchange ####
python3 ../lj3.py myprotein_hrpt.prmtop 16 4 -d 0.00 0.01 0.02 0.04 0.08 0.12 0.16 0.20 0.24 0.28 0.32 0.38 0.44 0.50 0.58 0.68 -e 1.00 0.99 0.98 0.97 0.96 0.94 0.92 0.90 0.88 0.86 0.84 0.82 0.80 0.78 0.76 0.74 -r ${r_mask} -l ${l_mask} -o system.top | tee lj.log

#### Energy Minimisation ####
pmemd.cuda -O -i ../min.in -c myprotein.rst -p myprotein_hrpt.prmtop -r min.rst -o min.out -x min.nc 

#### Heating Process #####
pmemd.cuda -O -i ../mdwat2.in -c min.rst -p myprotein_hrpt.prmtop -r mdwat2.rst -o mdwat2.out -ref myprotein.rst -x mdwat2.nc
pmemd.cuda -O -i ../mdwat3.in -c mdwat2.rst -p myprotein_hrpt.prmtop -r mdwat3.rst -o mdwat3.out -ref myprotein.rst -x mdwat3.nc
pmemd.cuda -O -i ../mdwat4.in -c mdwat3.rst -p myprotein_hrpt.prmtop -r mdwat4.rst -o mdwat4.out -ref myprotein.rst -x mdwat4.nc
pmemd.cuda -O -i ../mdwat5.in -c mdwat4.rst -p myprotein_hrpt.prmtop -r mdwat5.rst -o mdwat5.out -ref myprotein.rst -x mdwat5.nc
pmemd.cuda -O -i ../mdwat6.in -c mdwat5.rst -p myprotein_hrpt.prmtop -r mdwat6.rst -o mdwat6.out  -ref myprotein.rst -x mdwat6.nc

#### Running Replica Exchange ####
remd()
{
    groupfile=remd.groupfile
    for j in {1..16};
    do
        printf -- "-O -rem 3 -i ../remd.in -o remd_${j}.out -c mdwat6.rst -r remd_${j}.rst -x remd_${j}.nc -p syst_${j}.top -inf mdinfo.00${j} -l logfile.00${j}\n" >> "${groupfile}"
    done
    mpirun -np 16 pmemd.cuda_SPFP.MPI -O -ng 16 -groupfile ${groupfile}
}
remd
