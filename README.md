# Studying protein-protein interactions using an advanced sampling approach

## Modified Repulsive Scaling Replica Exchange MD Simulation

## Description
This is the master thesis of Yasmin Saremi. This work is based upon the study on "[Efficient Refinement and Free Energy Scoring of Predicted Protein–Protein Complexes Using Replica Exchange with Repulsive Scaling](https://doi.org/10.1021/acs.jcim.0c00853)" by Till Siebenmorgen and Martin Zacharias . This is meant to expand the methods discussed for explicit water simulations. Code examples for each simulation approach will be provided here.

## Installation
Clone this repo to your local computer.
```
git clone https://gitlab.com/ysaremi/master-thesis.git
```

Create a new python3 environment (e.g. using anaconda3)
```
conda create -n AmberTools20 python=3.9.13
conda activate AmberTools20
```
This simulation runs with the [AMBER 20](https://ambermd.org/doc12/Amber20.pdf) software package and the AmberTools20 package which should already be included in the first one. We used the amber 20.13/21.12 module on 1 GPU.

## REMD simulation
### Preperations
First, you need to create a file called mask.info. This needs to contain the information about the Ligand and Receptor Chain.
They must be written into mask.info like the following example:
```
:1-89
:90-185
```
The first line represents the receptor chain and the second the ligand. Please keep them in this format and only exchange the numbers. If you don't know the chain lengths, you can find them inside the amber.pdb file after you run your pdb file with the pdb4amber command.
```
pdb4amber -y -i <your-complex-pdb> amber.pdb
```
Look for the TER and END command and write the residue number down it is written with. These are your chain-ends.

### Run the Simulation
E.g You are interested in running the simulation for 1ay7.pdb, you would run this with:
```
cd rsremd-ff14SB
bash rsremd.sh 1ay7
```
### Forcefield Update
The forcefield update was done inside the leap.in and leap0.in files. They are the input files for the tleap program and can be changed and updated there. 
```
source leaprc.protein.ff14SB
source leaprc.water.opc
[...]
```
It is only necessary to swap the forcefield command line from ff14SB to ff19SB.
```
source leaprc.protein.ff19SB
source leaprc.water.opc
[...]
```
Everything else was kept identical.

### Modified Lennard-Jones Parameters

You can see the Lennard-Jones parameters inside the rsremd.sh file under
```
#### Creating Parm for Replica Exchange ####
python3 ../lj3.py myprotein_hrpt.prmtop 8 4 -d 0.00 0.02 0.08 0.16 0.24 0.32 0.44 0.68 -e 1.00 0.98 0.96 0.92 0.88 0.84 0.80 0.74 -r ${r_mask} -l ${l_mask} -o syst.top | tee lj.log
```
for the asremd simulations these were exchanged for the new modified parameters
```
#### Creating Parm for Replica Exchange ####
python3 ../lj3.py myprotein_hrpt.prmtop 8 4 -d 0.00 0.02 0.08 0.16 0.24 0.32 0.44 0.68 -e 1.00 0.98 0.96 0.92 0.88 0.84 0.80 0.74 -r ${r_mask} -l ${l_mask} -o syst.top | tee lj.log
```
Everything else about the simulations was kept identical. You can find the script in the asremd.sh file.

## Analysis
This was done using a jupyter notebook for easier control and access. We calculated the RMSD and have done an MBAR free energy calculation.
Open each .ipynb file in a jupyter notebook and rewrite the filepaths to point towards your files.

## Support
If you are interested in using the provided code examples and have further questons, please message me under _y.saremi@tum.de_.
If you want to make slight adjustments, I recommend you start with the rsremd.sh script since it is controlling the others.

## Authors and acknowledgment
Thanks for Till Siebenmorgen and Martin Zacharias. They provided the input files necessary and guided me through the whole process. The analysis code was written by Till Siebenmorgen and adjusted to the methods here discussed.
